<?php namespace Mambo\Cms\Core\Providers;

use Illuminate\Support\ServiceProvider;

abstract class CmsServiceProvider extends ServiceProvider{

    abstract public function register();
}
