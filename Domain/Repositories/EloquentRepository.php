<?php namespace Mambo\Cms\Core\Domain\Repositories;

use Mambo\Cms\Core\Domain\Entities\EloquentEntity;
use Mambo\Cms\Core\Domain\Repositories\CrudRepository;

abstract class EloquentRepository implements CrudRepository {

    protected $model;

    public function __construct(EloquentEntity $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        $this->model->all();
    }

    public function find($id)
    {
        return $this->find($id);
    }

    public function create(array $data)
    {
        return $this->create($data);
    }

    public function update($id, array $data)
    {
        $row = $this->model->find($id);
        if( !is_null($row) ){
            return $row->update($data);
        }

        return null;
    }

    public function delete($id)
    {
        return $this->model->delete($id);
    }

    public function search(array $where = array(), array $order = null, array $columns = array('*'))
    {
        $query = $this->model->select($columns);

        if( is_array($where) ){
            foreach( $order as $column => $direction ) {
                $query->model->orderBy($column, $direction);
            }
        }

        if( is_array($order) ){
            foreach( $order as $column => $direction ) {
                $query->model->orderBy($column, $direction);
            }
        }

        return $query->get();
    }
}
