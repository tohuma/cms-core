<?php namespace Mambo\Cms\Core\Domain\Entities;

use Illuminate\Database\Eloquent\Model;

abstract class EloquentEntity extends Model {

}
